#!/bin/sh
OCO_INSTALL_DIR=/tmp/oco-install
PATH=$PATH:$OCO_INSTALL_DIR/bin:$OCO_INSTALL_DIR/utils
mkdir -p $OCO_INSTALL_DIR/bin
mkdir -p $OCO_INSTALL_DIR/utils
mkdir -p $OCO_INSTALL_DIR/build
cd $OCO_INSTALL_DIR
VERSION=master
OCO_TAR_URL=https://gitlab.com/pLeonn/oco-core/-/archive/$VERSION/oco-core-$VERSION.tar.gz
if ! command -v wget &> /dev/null
then
    if ! command -v curl &> /dev/null
    then
        echo "curl or wget could not be found"
        exit
    else
        curl $OCO_TAR_URL -s -o oco-core-$VERSION.tar.gz 2> $OCO_INSTALL_DIR/install.log
    fi
else
wget $OCO_TAR_URL 2>&1 > $OCO_INSTALL_DIR/install.log
fi
mkdir -p oco-core && cd oco-core && tar xvfz ../oco-core-$VERSION.tar.gz  --strip-components 1 2>> $OCO_INSTALL_DIR/install.log
cd oco-adm &&
    cat params.inc.sh > $OCO_INSTALL_DIR/build/oco-adm.sh &&
    cat init.inc.sh >> $OCO_INSTALL_DIR/build/oco-adm.sh &&
    cat join.inc.sh >> $OCO_INSTALL_DIR/build/oco-adm.sh &&
    cat reset.inc.sh >> $OCO_INSTALL_DIR/build/oco-adm.sh &&
    cat main.sh >> $OCO_INSTALL_DIR/build/oco-adm.sh &&
    mv $OCO_INSTALL_DIR/build/oco-adm.sh $OCO_INSTALL_DIR/bin/oco-adm
    chmod +x $OCO_INSTALL_DIR/bin/oco-adm
cd ..
cd oco-cli &&
    cat params.inc.sh > $OCO_INSTALL_DIR/build/oco-cli.sh &&
    cat main.sh >> $OCO_INSTALL_DIR/build/oco-cli.sh &&
    mv $OCO_INSTALL_DIR/build/oco-cli.sh $OCO_INSTALL_DIR/bin/oco-cli
    chmod +x $OCO_INSTALL_DIR/bin/oco-cli
cd ..
cp uninstall.sh $OCO_INSTALL_DIR/bin/oco-uninstall 
chmod +x $OCO_INSTALL_DIR/bin/oco-uninstall 
cd ..

# --- use sudo if we are not already root ---
SUDO=sudo
if [ $(id -u) -eq 0 ]; then
    SUDO=
fi
$SUDO mv $OCO_INSTALL_DIR/bin/* /usr/local/bin/

echo " ------------------------------"
echo " ---- OCO Install Complete ----"
echo " ------------------------------"
echo " ------ Happy OCOing !! -------"
echo " ------------------------------"

rm -rf $OCO_INSTALL_DIR
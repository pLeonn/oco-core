
reset() {
    docker stop oco-api oco-scheduler oco-mgr oco-agent oco-db-0 
    docker rm -v oco-api oco-scheduler oco-mgr oco-agent oco-db-0
    docker network rm oco 
    rm -rf ~/.oco
}

#!/bin/sh

DIND=false
MGR_WORK=false
INIT=false
JOIN=false
JOIN_MGR=false
TOKEN=
API_URL=
RESET=false
# usage function
function usage()
{
cat << HEREDOC
    Usage: oco-adm [init] [join TOKEN] 

    optional arguments:
        --help                      show this help message and exit
        init                        init machine as oco master
            --dind                  init oco master container
            --allow-mgr-workloads   init oco master and allow workload to be scheduled to master

        join <TOKEN> <OCO API URL>  join an oco cluster
            --dind                  join as an oco worker container
            --manager               join as oco manager


HEREDOC
}  

if [ "$1" = "init" ]; then
    INIT=true
    if [ "$2" = "--allow-mgr-workloads" ]; then
    MGR_WORK=true
    fi
    if [ "$2" = "--allow-mgr-workloads" ] && [ "$3" = "--dind" ]; then
    MGR_WORK=true
    DIND=true
    fi
    if [ "$2" = "--dind" ]; then
    DIND=true
    fi
    if [ "$2" = "--dind" ] && [ "$3" = "--allow-mgr-workloads" ]; then
    MGR_WORK=true
    DIND=true
    fi
else
    if [ "$1" = "join" ]; then
        JOIN=true
        TOKEN=$2
        API_URL=$3
        if [ "$4" = "--manager" ]; then
        JOIN_MGR=true
        fi
        if [ "$4" = "--manager" ] && [ "$5" = "--dind" ]; then
        JOIN_MGR=true
        DIND=true
        fi
        if [ "$4" = "--dind" ]; then
        DIND=true
        fi
        if [ "$4" = "--dind" ] && [ "$5" = "--manager" ]; then
        JOIN_MGR=true
        DIND=true
        fi
    else
        if [ "$1" = "reset" ]; then
        RESET=true
        else
            if [ "$1" = "--help" ]; then
            usage
            else
                usage
            fi
        fi
    fi
fi
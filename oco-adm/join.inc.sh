
join() {
    mkdir -p ~/.oco/
    if [ -e ~/.oco/config ]
    then
        echo "OCO already exists in this environment, run 'oco-adm reset' to remove it. "
        exit 1
    fi
    echo "JOIN_TOKEN=$TOKEN" > ~/.oco/config
    docker network create oco
    docker run -d --restart always --name oco-agent --network oco -p 1087:8000 -e API_URL=$API_URL -e API_TOKEN=$TOKEN registry.gitlab.com/pleonn/oco-core/oco-agent:latest 
    echo " ------------------------------"
    echo " ----  OCO Join Complete   ----"
    echo " ------------------------------"
    echo " ------ Happy OCOing !! -------"
    echo " ------------------------------"
}


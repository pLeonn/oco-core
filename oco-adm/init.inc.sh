
init() {
    mkdir -p ~/.oco/db
    if [ -e ~/.oco/config ]
    then
        echo "OCO already exists in this environment, run 'oco-adm reset' to remove it. "
        exit 1
    fi
    JOIN_TOKEN=$(LC_ALL=C tr -dc 'A-Za-z0-9!"#$%&'\''()*+,-.;<=>?@[\]^_`{|}~' </dev/urandom | head -c 18 ;)
    echo "JOIN_TOKEN=$JOIN_TOKEN" > ~/.oco/config
    docker network create oco
    docker run -d -v ~/.oco/db:/data/db --restart always --name oco-db-0  --network oco mongo:4.4 
    docker run -d --restart always --name oco-api --network oco -p 1080:3000 -e API_TOKEN=$API_TOKEN registry.gitlab.com/pleonn/oco-core/oco-api:latest 
    docker run -d --restart always --name oco-mgr --network oco -e MGR_WORK=$MGR_WORK -e API_TOKEN=$API_TOKEN -p 1050:27017 registry.gitlab.com/pleonn/oco-core/oco-mgr:latest 
    docker run -d --restart always --name oco-scheduler --network oco -e API_TOKEN=$API_TOKEN registry.gitlab.com/pleonn/oco-core/oco-scheduler:latest 
    
    if [ $MGR_WORK = true ]; then
        docker run -d --restart always --name oco-agent --network oco -p 1087:8000 -e API_URL=http://oco-api:3000 -e API_TOKEN=$TOKEN registry.gitlab.com/pleonn/oco-core/oco-agent:latest 
    fi
    echo " ------------------------------"
    echo " ----  OCO Init Complete   ----"
    echo " ------------------------------"
    echo " ------ Happy OCOing !! -------"
    echo " ------------------------------"
}

const dns = require('dns')
const moment = require('moment')
const { ToadScheduler, SimpleIntervalJob, Task } = require('toad-scheduler')
var ping = require('ping');
const axios = require('axios');
const apiUrl = 'http://oco:' + process.env.API_TOKEN +'@oco-api:3000'

async function main(){ 
    const scheduler = new ToadScheduler()
    init()
    scheduler.addSimpleIntervalJob(new SimpleIntervalJob({ seconds: 2 }, 
        new Task('health check task', healthCheck)
    ))
    scheduler.addSimpleIntervalJob(new SimpleIntervalJob({ seconds: 2 }, 
        new Task('services check task', servicesCheck)
    ))

    if(process.env.MGR_WORK == false){
        scheduler.addSimpleIntervalJob(new SimpleIntervalJob({ seconds: 5 }, 
            new Task('heart beat', heartBeat)
        ))
    }
}

async function healthCheck() { 
    console.log('healthCheck');
    const timeout = process.env.HEARTBEAT_TIMEOUT || 50
    let nodes =[]
    try {
        let res = await axios.get(apiUrl + '/nodes');
        nodes = res.data
        for (let i = 0; i < nodes.length; i++) {
            let node = nodes[i]
            let alive = false
            for(let host of node.addrs){
                let res = await ping.promise.probe(host);
                alive = res.alive
                if (alive) 
                    break;
            }
            if(moment().diff(moment(node.updatedAt), 'seconds') > timeout) {
                await axios.post(apiUrl + '/nodes/' + node._id, {
                    status: alive ? 'NOTREADY' : 'UNREACHEBLE'
                });
            } else {
                if (node.status == "NOTREADY" || node.status ==  "UNREACHEBLE") {
                    await axios.post(apiUrl + '/nodes/' + node._id, {
                        status: alive ? 'READY' : 'UNREACHEBLE'
                    });
                }
            }
        }
    } catch (error) {
        console.error(error);
    }
}

async function servicesCheck() { 
    console.log('servicesCheck');
    const timeout = process.env.HEARTBEAT_TIMEOUT || 50
    let services = []
    try {
        let nodes = await axios.get(apiUrl + '/nodes');
        nodes = nodes.data;
        let res = await axios.get(apiUrl + '/services');
        services = res.data
        for (let i = 0; i < services.length; i++) {
            if (services[i].stage) {
                if (nodes.find(x => x._id === services[i].stage.node).status != 'READY') {
                    await axios.post(apiUrl + '/services/' + node._id, {
                    status: 'PENDING'
                });
                }
            }
        }
    } catch (error) {
        console.error(error);
    }
}

async function heartBeat(){ 
    console.log('heartBeat');
    try {
        let res = await axios.post(apiUrl + '/heartBeat', { addrs: await getIps() });
    } catch (error) {
        console.error(error);
    }
}

async function init(){ 
    try {
        await axios.post(apiUrl + '/nodes', { addrs: await getIps() });
    } catch (error) {
        console.error(error);
    }
}   

function getIps(){
    return new Promise(resolve => {
        dns.resolve('host.docker.internal', callback=(err, result) => resolve(result))
    });
};

main().catch(console.error);
 
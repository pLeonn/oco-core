
oco-adm init
oco-adm init --dind
oco-adm init --allow-mgr-workloads

oco-adm join <TOKEN> <OCO API URL>
oco-adm join <TOKEN> <OCO API URL> --manager
oco-adm reset

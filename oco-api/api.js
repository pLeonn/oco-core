
const express = require('express')
const basicAuth = require('express-basic-auth')
var mongoose = require("mongoose")
const DB = require('./db')

const port = 3000
const app = express()

app.use(basicAuth({
    users: { 'oco': process.env.API_TOKEN  }
}))
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.post('/heartBeat', async(req, res) => {
    res.send('ack')
    try {
        const db = await DB.getDB()
        var col = db.collection('nodes')
        const node = await col.findOne({addrs: req.body.addrs})
        
        await col.updateOne(
            {
                _id: node._id
            },
            {
                '$set': {
                    updatedAt: new Date()
                }
            }
        );
    } catch (err) {
        console.log(err);
    }
})

app.get('/nodes', async (req, res) => {
    const db = await DB.getDB()
    var col = db.collection('nodes')
    const nodes = await col.find().toArray()
    res.send(nodes)
})

app.post('/nodes', async (req, res) => {
    const db = await DB.getDB()
    var col = db.collection('nodes')
    var node  = await col.findOne({addrs: req.body.addrs})

    if (!node) {
        await col.insertOne({
            role: 'master',
            schedulable: process.env.MGR_WORK ? true : false,
            addrs: req.body.addrs,
            status: 'NOTREADY',
            createdAt: new Date(),
        });
    } 

    res.send('ack')
})

app.post('/nodes/:id', async(req, res) => {
    try {
        const db = await DB.getDB()
        var col = db.collection('nodes')
       let e =  await col.updateOne(
            {
                _id: mongoose.Types.ObjectId(req.params.id)
            },
            {
                '$set': req.body
            }
        );
    } catch (err) {
        console.log(err);
    }
    res.send('ok')
})

app.get('/services/:id', async(req, res) => {
    try {
        const db = await DB.getDB()
        var col = db.collection('services')
        let s =  await col.findOne(
            {
                _id: mongoose.Types.ObjectId(req.params.id)
            }
        );

        res.send(s)
    } catch (err) {
        console.log(err);
    }
})

app.post('/services/:id', async(req, res) => {
     try {
        const db = await DB.getDB()
        var col = db.collection('services')
        let e =  await col.updateOne(
            {
                _id: mongoose.Types.ObjectId(req.params.id)
            },
            {
                '$set': req.body
            }
        );
    } catch (err) {
        console.log(err);
    }

    res.send('ok')
})

app.get('/services', async(req, res) => {
    const db = await DB.getDB()
    var col = db.collection('services')
    const services = await col.find().toArray()
    res.send(services)
})

app.get('/services/pending', async(req, res) => {
    const db = await DB.getDB()
    var col = db.collection('services')
    const services = await col.find({status: 'PENDING'}).toArray()
    res.send(services)
})

app.post('/services', async(req, res) => {
    var service  = await col.findOne({description: req.body})

    if (!service) {
        await col.insertOne({
            description: req.body,
            status: 'PENDING'
        });

        res.send('pending')
    } else {
        res.send('rejected')
    }
})

app.get('/state', async(req, res) => {
    let nodes = []
    let services = []
    let containers = []
    try {
        const db = await DB.getDB()
        var col = db.collection('services')
        services = await col.find().toArray()
        col = db.collection('nodes')
        nodes = await col.find().toArray()
        col = db.collection('containers')
        containers = await col.find().toArray()
    } catch (err) {
        console.log(err);
    }    
    res.send({
        nodes: nodes,
        services: services,
        constainers: containers
    })
})

app.listen(port, () => {
  console.log(`OCO api listening at http://localhost:${port}`)
})
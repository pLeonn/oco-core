var mongoose = require("mongoose")
const uri = "mongodb://oco-db-0/oco";
const DB = {
    conn: null,
    getDB: async()=>{
        return await mongoose.createConnection(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    }
}

module.exports = DB

#!/bin/sh

# --- use sudo if we are not already root ---
SUDO=sudo
if [ $(id -u) -eq 0 ]; then
    SUDO=
fi

oco-adm reset
$SUDO rm /usr/local/bin/oco-adm
$SUDO rm /usr/local/bin/oco-cli
$SUDO rm /usr/local/bin/oco-uninstall
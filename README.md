
# OCO - Open Container Orchestrator

OCO is a Lightweight Container Orchestrator that runs over docker and can also be run Docker-in-Docker.

It is Great for:

Edge, IoT, CI, Development

1 - It has a reliable achitecture with suport for High availability clusters

2 - It is secure by default with reasonable defaults for lightweight environments.

3 - It has minimal to no OS dependencies (just Docker).

# Minimal Requirements

- Docker
- 10 - 50MB of RAM
- X CPU

Yeah, that is just it!

OCO requires approximately 1MB of RAM per 100 Resources managed.

OCO requires only 0.2 CPUs (but the more the better) for a good user experience.

# Quick-Start - Install Script
The install.sh script provides a convenient way to download and install OCO.

To install OCO-core componenents just run:

```sh
curl -sfL https://gitlab.com/pLeonn/oco-core/-/raw/master/install.sh | sh -
```

The install script will install OCO utilities, oco-cli, oco-adm and oco-uninstall.

then run
```sh
oco-adm init
```

The `oco-adm init` script will start the OCO componenents and a ococonfig file is written to ~/.oco/config.

That is it! you can see the node status by runnins `oco-cli get nodes`

Happy OCOing!

## OCO Client

The OCO Client (oco-cli) provides a way manage resources via command line

see the detailed [documentation](/docs/oco-cli.md)

## OCO Admin

OCO Admin (oco-adm) provides a easy way Create a OCO Cluster, Add & Drain nodes via command line

see the detailed [documentation](/docs/oco-adm.md)

# OCO Architecture

![ OCO Architecture](docs/arch.png "Title")

# OCO Components

### OCO Api
The oco-api component is reponsible for expose oco-core resources states and operations.

### OCO Scheduler
The oco-scheduler component is reponsible for evaluate the nodes statuses and properlly scheduling workloads.

### OCO Manager
The oco-mgr component is reponsible for managing oco resources and operations via oco-api.

### OCO Agent
The oco-agent component is reponsible for execute actions on the worker node.

# Roadmap

- oco-cli
- oco-Scheduler
- oco-agent
